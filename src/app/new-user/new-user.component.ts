import { Component, ElementRef, ViewChild } from '@angular/core';
import { UsersService } from '../shared/users.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent  {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('activeInput') activeInput!: ElementRef;
  @ViewChild('roleSelector') roleSelector!: ElementRef;

  constructor(private usersService: UsersService) { }

  addUser() {
     const name = this.nameInput.nativeElement.value;
     const email = this.emailInput.nativeElement.value;
     const active = this.activeInput.nativeElement.checked;
     const role = this.roleSelector.nativeElement.value;

     const newUser = new User(name, email, active, role);
     this.usersService.addUser(newUser);
  }
}
