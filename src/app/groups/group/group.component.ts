import { Component, Input, OnInit } from '@angular/core';
import { GroupService } from 'src/app/shared/group.service';
import { Group } from '../../shared/group.model';


@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  @Input() group!: Group;
  constructor(private groupService: GroupService) {
  }

  ngOnInit() {
  }

  onClick() {
    this.groupService.selectGroup(this.group);
  }

  changeClassInGroup(){
    if(this.groupService.currentGroup === this.group) {
      return 'selected';
    } else {
      return '';
    }
  }

}


