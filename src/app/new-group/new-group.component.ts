import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Group } from '../shared/group.model';
import { User } from '../shared/user.model';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css']
})
export class NewGroupComponent implements OnInit {
  @ViewChild ('inputGroup') inputGroup! : ElementRef;

  constructor(private groupService: GroupService) {}

  ngOnInit(): void {
  }

  addGroup(){
    const group: User[] = [];
    const groupName = this.inputGroup.nativeElement.value;
    const newGroup = new Group(groupName, group);
    this.groupService.addGroup(newGroup);
  }

}
