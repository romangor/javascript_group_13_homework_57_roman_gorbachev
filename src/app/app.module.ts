import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { UsersService } from './shared/users.service';
import { NewGroupComponent } from './new-group/new-group.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupService } from './shared/group.service';
import { GroupComponent } from './groups/group/group.component';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    UsersComponent,
    UserComponent,
    NewGroupComponent,
    GroupsComponent,
    GroupComponent,
    GroupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UsersService, GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
