import { EventEmitter } from '@angular/core';
import { Group } from './group.model';

export class GroupService {
  groupChange = new EventEmitter<Group[]>();
  groupSelect = new EventEmitter<Group>();
  currentGroup!: Group;

  groups: Group[] = [new Group('Work',[])];

  getGroups() {
    return this.groups.slice();
  }

  addGroup(group: Group) {
    this.groups.push(group);
    this.groupChange.emit(this.groups);
  }

  selectGroup(group: Group) {
    this.groupSelect.emit(group);
    this.currentGroup = group;
  }

}

