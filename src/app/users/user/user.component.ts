import { Component, Input } from '@angular/core';
import { User } from '../../shared/user.model';
import { GroupService } from '../../shared/group.service';
import { Group } from 'src/app/shared/group.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent  {
  @Input() user!: User;

  constructor(private groupService: GroupService) {}

  changeClass() {
    if(this.user.active){
      return 'online';
    } else {
      return 'offline';
    }
  }

  checkUserInGroup(group: Group) {
    return group.users.find(user => {
      return user.email === this.user.email;
    });
  }

  onClick() {
    const group = this.groupService.currentGroup;
    if(!group){
      alert("Please select a group");
    } else {
      const user = this.checkUserInGroup(group);
      if(user !== this.user){
        group.users.push(this.user);
      } else {
        alert('User is in the group');
      }
    }
  }
}
